FROM node:8.4-alpine
RUN apk add --no-cache bash gawk sed grep bc coreutils \
		mongodb 
VOLUME /data/db
WORKDIR /usr/src/app
COPY src .
RUN npm install
COPY scripts/run.sh /root
RUN chmod +x /root/run.sh
ENTRYPOINT [ "/root/run.sh" ]
EXPOSE 8080