#!/bin/bash
echo "Starting mongod"
mongod > /tmp/server-log.txt &

sleep 1
while ! grep -m1 'waiting for connections' < /tmp/server-log.txt; do
    sleep 1
done
node app.js $1