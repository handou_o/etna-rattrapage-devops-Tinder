var bodyParser = require('body-parser'); // Charge le middleware de gestion des paramètres
var express  = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var myschema = require('./schema/schema.js')
var http = require('http').Server(app);
//Connect to the db


var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost', { useMongoClient: false } , function(err) {
    if (err) {
		console.log("connexion a la base de donnees impossible ");
	}
});

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
	
});
app.use(express.static(__dirname + '/views'));
app.use(bodyParser.json());
app.get("/", function(req, res) {
	res.render('./index.html');
});

var args = process.argv.splice(process.execArgv.length + 2);

if (args[0] == "test"){
var testInbox = require('./Test/testInbox.js');
var testMatch = require('./Test/testMatch.js');
var testUser = require('./Test/testUser.js');

}
require('./Services/User.js')(app,mongoose,myschema);
require('./Services/Match.js')(app,mongoose,myschema);
require('./Services/Inbox.js')(app,mongoose,myschema);
/* On utilise les sessions */
app.listen(8080); 
db.once('open', function () {
	console.log("db connected");
	if (args[0] == "test"){
	/* Test inbox */
	testInbox.postInbox(mongoose,mongoose.model('Message', myschema.Message),testInbox.getInboxId);
	/* Test Match */
	testMatch.postLikeId(mongoose, mongoose.model('Like', myschema.Like),testMatch.getLikeId);
	testMatch.postCaracteristicId(mongoose,mongoose.model('Caracteristic', myschema.Caracteristic),testMatch.getCaracteristicId);
	testMatch.postMatchId(mongoose,mongoose.model('Match', myschema.Match),testMatch.getMatchId);
	/* Test User */
	testUser.postUser(mongoose,mongoose.model('User', myschema.User),testUser.getUserId);
	setTimeout(function(){ process.exit(0) }, 5000);
	}
	
});

