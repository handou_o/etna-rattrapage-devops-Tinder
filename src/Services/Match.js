
module.exports = function(app,mongoose,myschema){
/* ############################################
	#################### LIKE #################
	###########################################
*/
app.get('/like/:id', function(req, res) {
        var id = req.params.id;

        var likeModel = mongoose.model('Like', myschema.Like);
        likeModel.find({
            likerId: id
        }, function(err, likes) {
            if (err) {
                throw err
            }
            res.send(likes);
        });
    })

    .post('/like/:id', function(req, res) {
        var id = req.params.id;
		var status = req.body.status;
        var user_id = req.body.user_id;
		
		if (id.length == 0 || status.length == 0 || user_id.length == 0){
		res.status(400);
		res.send("invalid input, object invalid");
		next();
		}
        var likeModel = mongoose.model('Like', myschema.Like);
		likeModel.find({
			user_id: req.body.user_id
		}, function(err, likes) {
			if (err) {
				throw err
			}
			if (!likes.length) {
				var like = new likeModel({
					status: status,
					likerId: id,
					user_id: user_id
				});
				like.save(function(err) {
					if (err) {
						throw err;
						res.status(400);
						res.send("invalid input, object invalid");
					}
					else{
						res.status(201).send("item created");
					}
				});
			} else {
				res.status(409);
				res.send("an existing item already exists");
			}

		});
    })

    /* ############################################
    	#################### RECOMMANDATION #################
    	###########################################
    */
    .post('/recommandation', function(req, res) {
            var caraModel = mongoose.model('Caracteristic', myschema.Caracteristic);
			var age_min = req.body.age_min;
			var age_max = req.body.age_max;

			if (age_max == undefined || age_max.length == 0  || age_min == undefined || age_min.length == 0 ){
				res.status(400);
				res.send("invalid input, object invalid");
				next();
			}
			age_min = parseInt(age_min);
			age_max = parseInt(age_max);
			 
			 var minDate = new Date();
			 var maxDate = new Date();
			 minDate.setFullYear( minDate.getFullYear() - age_min );
			 maxDate.setFullYear( maxDate.getFullYear() - age_max - 1 );
			 console.log(maxDate);
			 			 console.log(minDate);
            caraModel.find({
                birth_date: {
                    $gt: maxDate,
                    $lt: minDate
                },
                gender: req.body.gender
            }, function(err, users) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                res.status(200);
                res.send(users);
            });
        })

        /* ############################################
        	#################### CARACTERISTIQUE #################
        	###########################################
        */

        .get('/recommandation/:id/caracteristic', function(req, res) {
            var id = req.params.id;

            var caraModel = mongoose.model('Caracteristic', myschema.Caracteristic);
            caraModel.find({
                user_id: id
            }, function(err, caras) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                res.status(200);
                res.send(caras);
            });
        })

        .post('/recommandation/:id/caracteristic', function(req, res) {
            var id = req.params.id;
			var user_id = req.body.user_id;
			var birth_date = req.body.birth_date;
			var gender = req.body.gender;


			if (user_id == undefined || user_id.length == 0  || birth_date == undefined || birth_date.length == 0 ||
			gender == undefined || gender.length == 0 ){
				res.status(400);
				res.send("invalid input, object invalid");
				return;
			}

			
			if (isNaN(Date.parse(birth_date))){
				res.status(400);
				res.send("invalid input, object invalid");
				return;
			}
				birth_date = new Date(birth_date);
            var caraModel = mongoose.model('Caracteristic', myschema.Caracteristic);

            caraModel.find({
                user_id: id
            }, function(err, cara) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                if (!cara.length) {
                    var newcara = new caraModel({
                        user_id: req.body.user_id,
                        bio: req.body.bio,
                        birth_date: birth_date,
                        gender: req.body.gender
                    });
                    /* Manque photoooo */

                    newcara.save(function(err) {
                        if (err) {
							throw err;
                            res.status(400);
                            res.send("invalid input, object invalid");
                        }
						else{
						     res.status(201);
							res.send("Item created");
						}

                    });
                } else {
                    res.status(400);
                    res.send("item already exist with user id:" + req.body.user_id);
                }
            });


        })

        .put('/recommandation/:id/caracteristic', function(req, res) {
			  var id = req.params.id;
			var user_id = req.body.user_id;
			var birth_date = req.body.birth_date;
			var gender = req.body.gender;


			if (user_id == undefined || user_id.length == 0  || birth_date == undefined || birth_date.length == 0 ||
			gender == undefined || gender.length == 0 ){
				res.status(400);
				res.send("invalid input, object invalid");
				return;
			}
				if (isNaN(Date.parse(birth_date))){
				res.status(400);
				res.send("invalid input, object invalid");
				return;
			}
				birth_date = new Date(birth_date);
			
            var caraModel = mongoose.model('Caracteristic', myschema.Caracteristic);

            caraModel.find({
                user_id: id
            }, function(err, caras) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                if (!caras.length) {
                    res.status(400);
                    res.send("invalid input,no (item exist with user id:" + req.body.user_id + " )");
                } else {
                    /* Manque photoooo */
					var cara = caras[0];
                    cara.bio = req.body.bio;
                    cara.birth_date = birth_date;
                    cara.gender = gender;
                    cara.save(function(err, updatecara) {
                        if (err) {
                            res.status(400);
                            res.send("invalid input, object invalid");
                        }
						else 
						{
							            res.status(201);
            res.send("Item created");
						}
                    });
                }
            });



        })
        /* ############################################
        	#################### MATCH #################
        	###########################################
        */
        .get('/match', function(req, res) {
			var skip = req.query.skip;
			var limite = req.query.limite;
			if (skip == undefined || limite == undefined )
			{
				skip = 0;
				limite = 0;
			}
			else{
				skip = parseInt(skip)
				limite = parseInt(limite)
				if (isNaN(skip))
				skip = 0;
				if (isNaN(limite))
				limite = 0;
			}
            var matchModel = mongoose.model('Match', myschema.Match);
            var query = matchModel.find({}).skip(skip).limit(limite);
			query.exec(function(err, matchs) {
                if (err) {
                    throw err
                }
                res.send(matchs);
            });
        })

        .get('/match/:id', function(req, res) {
            var id = req.params.id;
			var ids = id.split(",");
            var matchModel = mongoose.model('Match', myschema.Match);
            matchModel.find({
                match_id: ids
            }, function(err, caras) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                res.status(200);
                res.send(caras);
            });
        })
        .post('/match', function(req, res) {
			
			var match_id = req.body.match_id;
			var first_user_id = req.body.first_user_id;
			var second_user_id = req.body.second_user_id;


			if (match_id == undefined || match_id.length == 0  || first_user_id == undefined || first_user_id.length == 0 ||
			second_user_id == undefined || second_user_id.length == 0 ){
				res.status(400);
				res.send("invalid input, object invalid");
				console.log("error1");
				return;
			}
            var matchModel = mongoose.model('Match', myschema.Match);

            matchModel.find({
                match_id: match_id
            }, function(err, matchs) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                if (!matchs.length) {
                    var newmatch = new matchModel({
                        match_id: match_id,
                        first_user_id: first_user_id,
                        second_user_id: second_user_id,
                        match_date: new Date()
                    });
                    newmatch.save(function(err) {
                        if (err) {
							throw err;
								console.log("error2");
                            res.status(400);
                            res.send("invalid input, object invalid");
                        }else{
						
						res.status(201);
						res.send("Item created");
						}
                    });
                } else {
                    res.status(400);
                    res.send("item already exist with match id:" + match_id);
                }
            });

        });
}