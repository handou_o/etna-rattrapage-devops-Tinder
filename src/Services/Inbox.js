module.exports = function(app,mongoose,myschema){
	app.get('/inbox', function(req, res) {
				var skip = req.query.skip;
			var limite = req.query.limite;
        var messageModel = mongoose.model('Message', myschema.Message);
        // messageModel.find({}, "match_id", function(err, users) {
            // if (err) {
                // res.status(400);
                // res.send("Bad input parameter");
            // }
            // res.send(users);
        // });
		
		messageModel.aggregate([
        {
            $group: {
                _id: '$match_id',  //$region is the column name in collection
                count: {$sum: 1}
            }
		},
		{
			$project: {  
				"_id": 0,
				"match_id":"$_id"
			}
		}	
    ], function (err, result) {
			if (err) {
				throw err;
            next(err);
        } else {
                res.send(result);
        }
    });
    })
	 .get('/inbox/:id', function(req, res) {
         var id = req.params.id;

        var messageModel = mongoose.model('Message', myschema.Message);
            messageModel.find({
                match_id: id
            }, function(err, caras) {
                if (err) {
                    res.status(400);
                    res.send("Bad input parameter");
                }
                res.status(200);
                res.send(caras);
            });
        })
	.post('/inbox/:id', function(req, res) {
		

			var id = req.body.id;
			var req_from = req.body.from;
			var to = req.body.to;
			var match_id = req.params.id;

			if (id == undefined || id.length == 0  || 
			req_from == undefined || req_from.length == 0 ||
			to == undefined || to.length == 0 ||
			match_id == undefined || match_id.length == 0 ){
				res.status(400);
				res.send("invalid input, object invalid");
				return;
			}
			

        var messageModel = mongoose.model('Message', myschema.Message);
        messageModel.findById(id, function(err, messages) {
            if (err) {
                res.status(400);
                res.send("Bad input parameter");
            }
			console.log(messages);
            if (messages == null || messages.length == 0) {
                var newmessage = new messageModel({
                    _id: id,
                    from: req.body.from,
                    to: req.body.to,
                    match_id: req.body.match_id,
                    content: req.body.content,
                    sent_date: new Date(),
                    created_date: new Date()
                });

                newmessage.save(function(err) {
                    if (err) {
						throw err;
                        res.status(400);
                        res.send("invalid input, object invalid");
                    }
					else{
						  res.status(201);
						res.send("Item created");
					}
                });
            } else {
                res.status(409);
                res.send("an existing item already exists");
            }
        });
    })
    .post('/inbox', function(req, res) {
			var id = req.body.id;
			var req_from = req.body.from;
			var to = req.body.to;
			var match_id = req.body.match_id;


			if (id == undefined || id.length == 0  || 
			req_from == undefined || req_from.length == 0 ||
			to == undefined || to.length == 0 ||
			match_id == undefined || match_id.length == 0 ){
				res.status(400);
				res.send("invalid input, object invalid");
				return;
			}
			

        var messageModel = mongoose.model('Message', myschema.Message);
        messageModel.findById(id, function(err, messages) {
            if (err) {
                res.status(400);
                res.send("Bad input parameter");
            }
            if (messages == null || messages.length == 0) {
                var newmessage = new messageModel({
                    _id: id,
                    from: req.body.from,
                    to: req.body.to,
                    match_id: req.body.match_id,
                    content: req.body.content,
                    sent_date: new Date(),
                    created_date: new Date()
                });

                newmessage.save(function(err) {
                    if (err) {
						throw err;
                        res.status(400);
                        res.send("invalid input, object invalid");
                    }
					else{
						  res.status(201);
						res.send("Item created");
					}
                });
            } else {
                res.status(409);
                res.send("an existing item already exists");
            }
        });
     
    });
	

}