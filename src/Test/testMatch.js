var http = require('http');
var assert = require('assert') 
var id = "testtest";
var likeValue = 1;
module.exports = {
	postLikeId : function test1(mongoose,model,func){
		model.findOneAndRemove({ user_id: id }).exec();
		var jsonObject = JSON.stringify({
			  user_id: id,
			  status: likeValue
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/like/'+id,
		    method: "POST",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				console.log(data);
				assert.deepEqual(response.statusCode, 201, "Create Like Failed (Status code)!");
				console.log("Create Like Success !" );
				func();
			});
		});
		req.on('error', function(err) {
			console.log("Create Like Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	},
	postCaracteristicId : function test1(mongoose,model,func){
		model.findOneAndRemove({ user_id: id }).exec();
		var jsonObject = JSON.stringify({
			  user_id: id,
			bio: "I am an actor",
			birth_date: "10/12/1990",
			gender: 1
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/recommandation/'+id+'/caracteristic',
		    method: "POST",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				console.log(data);
				assert.deepEqual(response.statusCode, 201, "Create Caracteristic Failed (Status code)!");
				console.log("Create Caracteristic Success !" );
				func();
			});
		});
		req.on('error', function(err) {
			console.log("Create Caracteristic Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	},
	putCaracteristicId : function test1(){
		var date = new Date();
		date.setFullYear( date.getFullYear() - 20 ); 
		var jsonObject = JSON.stringify({
			  user_id: id,
			bio: "I am an actorsss",
			birth_date: date.getMonth() + "/" +  date.getDate() + "/" +date.getFullYear() ,
			gender: 1
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/recommandation/'+id+'/caracteristic',
		    method: "PUT",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				console.log(data);
				assert.deepEqual(response.statusCode, 201, "Update Caracteristic Failed (Status code)!");
				console.log("Update Caracteristic Success !" );
				module.exports.postSearchRecommandation();
			});
		});
		req.on('error', function(err) {
			console.log("Update Caracteristic Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	},
	getCaracteristicId : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/recommandation/'+id+'/caracteristic',
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get Caracteristic Failed !" );
					process.exit()
				}
				console.log("Get Caracteristic Success !" );
				module.exports.putCaracteristicId();

			});
		});
		req.on('error', function(err) {
			console.log("Get Caracteristic Failed !" );
			process.exit()
		});

		req.end();
	},
	getLikeId : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/like/' + id
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get Like Failed !" );
					process.exit()
				}
				console.log("Get Like Success !" );
	
			});
		});
		req.on('error', function(err) {
			console.log("Get Like Failed !" );
			process.exit()
		});

		req.end();
	},
	postMatchId : function test1(mongoose,model,func){
		model.findOneAndRemove({ match_id: id }).exec();
		var jsonObject = JSON.stringify({
		 match_id: id,
		 first_user_id: id,
		 second_user_id: id
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/match',
		    method: "POST",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				console.log(data);
				assert.deepEqual(response.statusCode, 201, "Create Match Failed (Status code)!");
				console.log("Create Match Success !" );
				func();
			});
		});
		req.on('error', function(err) {
			console.log("Create Match Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	},
	getMatchId : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/match/' + id
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get Match Id Failed !" );
					process.exit()
				}
				console.log("Get Match Id Success !" );
				module.exports.getAllMatch();	
			});
		});
		req.on('error', function(err) {
			console.log("Get Match Id Failed !" );
			process.exit()
		});

		req.end();
	}
	,getAllMatch : function test1(){
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/match?skip=0&limite=1'
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});

			response.on('end', function() {
				var result = JSON.parse(data);
				if (result.length < 1){
					console.log("Get All Match Failed !" );
					process.exit()
				}
				console.log("Get All Match Success !" );
	
			});
		});
		req.on('error', function(err) {
			console.log("Get All Match Failed !" );
			process.exit()
		});

		req.end();
	},
	postSearchRecommandation : function test1(){
		var age = new Date().getFullYear() + 20 - new Date().getFullYear() ;
		
		var jsonObject = JSON.stringify({
			age_max: age,
			age_min: age,
			gender: 1
		});
		var req = http.request({
			hostname: '127.0.0.1',
			port: '8080',
			path: '/recommandation',
		    method: "POST",
			    headers: {
				"Content-Type": "application/json",
				"Content-Length": Buffer.byteLength(jsonObject)
				},
			body: jsonObject
		}, function(response) {
			var data = '';
			response.on('data', function(chunk) {
				data += chunk;
			});
			response.on('end', function() {
				var result = JSON.parse(data);
				assert.deepEqual(response.statusCode, 200, "Get Recommandation Failed (Status code)!");
				if (result.length < 1){
					console.log("Get Recommandation Failed !" );
					process.exit()
				}
				console.log("Get Recommandation Success !" );
			});
		});
		req.on('error', function(err) {
			console.log("Get Recommandation Failed !" );
			process.exit()
		});
		req.end(jsonObject);
	},
	
	
}