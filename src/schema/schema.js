var mongoose = require('mongoose');
var Schema = mongoose.Schema;

module.exports.User = new Schema({
  firstName: { type: String, required: true},
  lastName: { type: String, required: true },
  email: { type: String, required: true },
  _id: {type: String, required: true ,unique: true}, 
  created_date: Date
});
module.exports.User.set('toJSON', {
			transform: function (doc, ret, options) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
			}
}); 

module.exports.Message = new Schema({
	_id: {type: String, required: true,unique: true}, 
	from: { type: String, required: true},
	to: { type: String, required: true},
	match_id: { type: String, required: true},
	created_date: Date,
	sent_date: Date
	});
	
module.exports.Message.set('toJSON', {
			transform: function (doc, ret, options) {
			ret.id = ret._id;
			delete ret._id;
			delete ret.__v;
			}
}); 

module.exports.Match = new Schema({
	match_id : { type: String, required: true},
	first_user_id: { type: String, required: true},
	second_user_id : { type: String, required: true},
	match_date: Date
});

module.exports.Match.set('toJSON', {
			transform: function (doc, ret, options) {
			delete ret._id;
			delete ret.__v;
			}
}); 
module.exports.Like = new Schema({
	likerId: {type: String, required: true}, 
	user_id:{type: String, required: true}, 
	status: { type: String, required: true}
});
module.exports.Like.set('toJSON', {
			transform: function (doc, ret, options) {
			delete ret._id;
			delete ret.__v;
			}
});


module.exports.Caracteristic = new Schema({
	  user_id: {type: String, required: true,unique: true}, 
	  bio: String,
	  birth_date: Date,
	  gender: Number
});

module.exports.Caracteristic.set('toJSON', {
			transform: function (doc, ret, options) {
			delete ret._id;
			delete ret.__v;
			}
});
